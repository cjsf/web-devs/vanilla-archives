# CJSF's Vanilla Archives

CJSF 90.1 FM produces recordings of all content that is broadcast from its radio tower. 

These recordings are created automatically by CJSF's web server by using Darkice to capture audio and a script called `logjam2.pl` to make the files.

These files are available at https://vanilla.cjsf.ca and can even be accessed while it is recording. Recordings are stored permanently as files with a total duration of 30 minutes each.

Vanilla Archive files are also used by CJSF's installation of DJ Land in order to allow DJ's to produce Podcast files from their digital logsheets. 
